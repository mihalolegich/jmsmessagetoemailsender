package com.getjavajob.shipunovm.socialnetwork.jms;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Arrays;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Document("socnetworkmessages")
public class SocNetworkJmsMessage {

    @Id
    private String id;
    private String messageText;
    private List<String> emails;

    @Override
    public String toString() {
        return "SocNetworkJmsMessage{" +
                "messageText='" + messageText + '\'' +
                ", emails=" + Arrays.toString(emails.toArray()) +
                '}';
    }

}
