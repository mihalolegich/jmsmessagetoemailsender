package com.getjavajob.shipunovm.jmstoemailsender.mongo;

import com.getjavajob.shipunovm.socialnetwork.jms.SocNetworkJmsMessage;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SocNetworkJmsMessageRepository extends MongoRepository<SocNetworkJmsMessage, String> {

}
