package com.getjavajob.shipunovm.jmstoemailsender.jms.consumers;

import com.getjavajob.shipunovm.jmstoemailsender.jms.messageprocessor.JmsMessageProcessor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import javax.jms.Message;
import javax.jms.MessageListener;

@Component
@Slf4j
public class JmsConsumer implements MessageListener {

    @Autowired
    private JmsMessageProcessor processor;

    @Override
    @JmsListener(destination = "${active-mq.topic}")
    public void onMessage(Message message) {
        log.debug("Received jms message: " + message.toString());
        processor.process(message);
    }

}