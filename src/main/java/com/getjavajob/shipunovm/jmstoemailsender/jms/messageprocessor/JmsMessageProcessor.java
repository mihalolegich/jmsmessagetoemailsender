package com.getjavajob.shipunovm.jmstoemailsender.jms.messageprocessor;

import javax.jms.Message;

public interface JmsMessageProcessor {

    void process(Message message);

}
