package com.getjavajob.shipunovm.jmstoemailsender.jms.messageprocessor;

import com.getjavajob.shipunovm.jmstoemailsender.emailing.EmailService;
import com.getjavajob.shipunovm.jmstoemailsender.mongo.SocNetworkJmsMessageRepository;
import com.getjavajob.shipunovm.socialnetwork.jms.SocNetworkJmsMessage;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.support.converter.MessageConverter;
import org.springframework.stereotype.Component;

import javax.jms.JMSException;
import javax.jms.Message;

@Slf4j
@Component
public class JmsMessageToEmailSender implements JmsMessageProcessor {

    @Autowired
    private EmailService emailService;

    @Autowired
    private SocNetworkJmsMessageRepository messageRepository;

    @Autowired
    private MessageConverter messageConverter;

    @Override
    public void process(Message message) {
        try {
            SocNetworkJmsMessage socNetworkJmsMessage = (SocNetworkJmsMessage) messageConverter.fromMessage(message);
            messageRepository.save(socNetworkJmsMessage);
            log.info("test message id is " + socNetworkJmsMessage.getId());
            for (String email : socNetworkJmsMessage.getEmails()) {
                emailService.sendSimpleMessage("eventSender@soc-network.com",
                        email,
                        "social network event",
                        socNetworkJmsMessage.getMessageText());
            }
        } catch (JMSException e) {
            log.error("jms message conversion exception");
        }
    }

}
